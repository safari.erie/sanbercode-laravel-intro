<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
     /**
     * method register
     * return register page
     */
    public function register(){
        return view('register');
    }
      /**
     * method register
     * return welcome
     * @param (all req from form)
     */
    public function welcome(Request $request){
        $data = array(
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'genders' => $request->genders,
            'nationality' => $request->nationality,
            'languages' => $request->languages,
            'bio' => $request->bio,
        );

        return view('welcome',['data' => $data]);
    }
}
