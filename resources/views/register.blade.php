
@extends('layout.main')
@section('title','Register')
@section('container')
    <div class="container">
        <h1>Buat Account Baru</h1>
    <br />
    <h2>Sign Up Form</h2>
    <br />
    <form method="POST" action="/welcome">
        @csrf
      <label for="firstname">First Name :</label>
      <br />
      <input type="text" id="firstname" name="firstname" class="form-control" /><br />
      <br />
      <label for="lastname"> Last Name</label>
      <br />
      <input type="text" id="lastname" name="lastname" class="form-control"/>
      <br />
      <br />
      <label for="gender"> Gender : </label>
      <br /><br />
      <input type="radio" name="genders" value="male" /> Male <br />
      <input type="radio" name="genders" value="femele" /> Female <br />
      <input type="radio" name="genders" value="others" /> Others <br />
      <br />
      <label for="nationality"> National</label>
      <select name="nationality" class="form-control">
        <option value="0"> Pilih </option>
        <option value="IND"> Indonesia </option>
        <option value="ENG"> English </option>
        <option value="MALAY"> MALAY </option>
      </select>
      <label for="languange"> Language Spoken : </label>
      <br /><br />
      <input type="checkbox" name="languages" value="ind" /> Bahasa Indonesia <br />
      <input type="checkbox" name="languages" id="eng" /> English <br />
      <input type="checkbox" name="languages" id="etc" /> Other <br />
      <br />
      <label for="bio"> Bio : </label>
      <br />
      <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
      <br />
      <input type="submit" value="Sign Up" class="btn btn-info" />
    </div>
@endsection
