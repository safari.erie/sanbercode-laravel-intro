@extends('layout.main')
@section('title','Welcome Page')
@section('container')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="mt-3"><strong>SELAMAT DATANG  {{$data['firstname']}}  {{$data['lastname']}}</strong></h1>
            <h2>Jenis Kelamin : {{$data['genders']}}</h2>
            <h2>Kebangsaan : {{$data['nationality']}}</h2>
            <h2>Keahlian Bahasa : {{$data['languages']}}</h2>
            <h2>Bio : {{$data['bio']}}</h2>
             <h2>
                Terima kasih telah bergabung di Sanbercode. Social Media Kita bersama!
            </h2>
        </div>
    </div>
</div>

@endsection
